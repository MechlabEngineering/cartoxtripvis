# CartoXTripVis

Deck.gl Frontend to visualize Car-2-X Communication Trips

![screenshot](screenshot.png)

Add your own Mapbox Access token in the `/html/script.js` file. Load a valid GeoJSON file with LineString Features from your local computer to visualize Car-2-X Communication Trips.