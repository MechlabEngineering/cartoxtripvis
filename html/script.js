var MAPBOX_ACCESS_TOKEN = 'YOUR_MAPBOX_ACCESS_TOKEN_HERE';
var map;
var c2cdata = [];

$(document).ready(function () {

    document.getElementById('fileinput').addEventListener('change', readSingleFile, false);

    $.when(
        // Load the Buildings Data
        $.ajax({
            type: "GET",
            url: "buildings-dresden.geojson",
            dataType: "JSON",
            success: function (data) {
                buildingsdata = data;
            }
        })
    ).then(function () {
        hidePreloader();
        map = createMap(buildingsdata);
    });
});

function readSingleFile(evt) {

    if (!evt || !evt.target || !evt.target.files || evt.target.files.length === 0) {
        alert("Failed to load file");
        console.error(evt);
        return;
    } else {
        var f = event.target.files[0]
        var name = f.name;
        var type = f.type;
        var lastDot = name.lastIndexOf('.');
        var ext = name.substring(lastDot + 1);
        var filesizeMB = f.size / 1024 / 1024;
    }

    // Quelle: https://www.htmlgoodies.com/beyond/javascript/read-text-files-using-the-javascript-filereader.html
    if (ext.toLowerCase() == 'geojson') {
        console.log(f);

        if (filesizeMB < 100.0) {
            var r = new FileReader();
            r.onload = function (e) {
                var contents = e.target.result;
                try {
                    // Process the GeoJSON file
                    geojson = JSON.parse(contents)
                    console.log('Loading ' + geojson.features.length + ' Features');
                    features = geojson.features;

                    newc2cdata = geojsonfeatures2object(features);
                    updatemap(newc2cdata);

                } catch (err) {
                    alert("Fehler beim Laden von " + f.name);
                    console.error(err);
                }
            }
            r.readAsText(f);

        } else {
            alert('100MB GeoJSON File Size Limit');
        }

    } else {
        alert(f.name + " is not a geojson file");
        console.log(ext.toLowerCase())
        console.error('Filetype: ' + f.type);
    }
}

// GeoJSON Converter Function
function geojsonfeatures2object(features) {
    c2cdata = []; // Clear the Array

    for (var i = 0; i < features.length; i++) {
        var feature = features[i];
        var obj = {};

        if (feature.geometry.type == 'LineString') {
            obj = feature.properties;
            var c = feature.geometry.coordinates;
            if (c.length > 1) {
                obj['sender_lon'] = c[0][0];
                obj['sender_lat'] = c[0][1];
                obj['receiver_lon'] = c[1][0];
                obj['receiver_lat'] = c[1][1];
            } else {
                alert('GeoJSON Linestring needs 2 coordinates')
                break;
            }
        } else {
            alert('GeoJSON Geometry Type not supported')
            console.log(feature.geometry.type);
            break;
        }
        c2cdata.push(obj);
    }
    return c2cdata;
}



// Main Function to create the Map
function createMap(buildingsdata) {

    const buildingslayer = new deck.GeoJsonLayer({
        id: 'buildings',
        data: buildingsdata,

        extruded: true,
        wireframe: false,
        opacity: 1.0,
        getElevation: function (f) {
            // Feature for every building
            var height = 0.0;
            if (f.properties['building:levels']) {
                height = f.properties['building:levels'] * 3.5;
            } else if (f.properties['height']) {
                height = parseFloat(f.properties['height'])
            } else {
                height = 6.0;
            }
            return height;
        },
        getFillColor: [113, 104, 90]
    });

    const map = new deck.DeckGL({
        mapboxApiAccessToken: MAPBOX_ACCESS_TOKEN,
        mapStyle: 'mapbox://styles/mapbox/light-v9',
        initialViewState: {
            longitude: 13.74,
            latitude: 51.05,
            zoom: 12,
            pitch: 0
        },
        controller: true,
        layers: [buildingslayer]
    });
    return map;
}

function updatemap(c2cdata) {

    maplayers = map.props.layers;
    layerid = maplayers.length + 1;
    console.log('Adding Layer with id: ' + 'c2cdata' + layerid);

    const c2clayer = new deck.ArcLayer({
        id: 'c2cdata' + layerid,
        data: c2cdata,

        // Styles
        getSourcePosition: f => [f.sender_lon, f.sender_lat],
        getTargetPosition: f => [f.receiver_lon, f.receiver_lat],
        getSourceColor: [148, 198, 0],
        getTargetColor: [113, 104, 90, 10],
        highlightColor: [255, 103, 0],
        getWidth: 8,

        pickable: true,
        autoHighlight: true,
        onHover: info => setTooltip(info.object, info.x, info.y)
    });

    map.setProps({ layers: [maplayers, c2clayer] });
}

function setTooltip(object, x, y) {

    const el = document.getElementById('tooltip');

    if (object) {
        geojsonfeatures = '';
        for (let [key, value] of Object.entries(object)) {
            geojsonfeatures += '<tr><td>' + key + ':</td><td>' + value + '</td></tr>';
        }
        el.innerHTML = '<table>' + geojsonfeatures + '</table>';
        el.style.display = 'block';
        el.style.left = x + 'px';
        el.style.top = y + 'px';
    } else {
        el.style.display = 'none';
    }
}

function showPreloader() {
    document.getElementById("preloader").style.display = "block";
}
function hidePreloader() {
    document.getElementById("preloader").style.display = "none";
}
